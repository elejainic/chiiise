require 'rails_helper'
include Warden::Test::Helpers

describe "User" do

  before(:each) do
    @user = FactoryGirl.create(:user)
    login_as @user, :scope => :user
  end

    describe "microtabla associations" do

    before { @user.save }
    let!(:older_microtabla) do
      FactoryGirl.create(:microtabla, user: @user, created_at: 1.day.ago)
    end
    let!(:newer_microtabla) do
      FactoryGirl.create(:microtabla, user: @user, created_at: 1.hour.ago)
    end

    it "should have the right microposts in the right order" do
      expect(@user.microtablas.to_a).to eq [newer_microtabla, older_microtabla]
    end

    it "should destroy associated microtablas" do
      microtablas = @user.microtablas.to_a
      @user.destroy
      expect(microtablas).not_to be_empty
      microtablas.each do |microtabla|
        expect(Microtabla.where(id: microtabla.id)).to be_empty
      end
    end
  end
end