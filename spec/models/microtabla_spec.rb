
require 'rails_helper'


describe "Microtabla" do
  let(:user) { FactoryGirl.create(:user) }
  before { @microtabla = user.microtablas.build(content: "Lorem ipsum") }

  subject { @microtabla }

  it { should respond_to(:content) }
  it { should respond_to(:user_id) }
  it { should respond_to(:user) }

  it { should be_valid }

  describe "when user_id is not present" do
    before { @microtabla.user_id = nil }
    it { should_not be_valid }
  end

  describe "with blank content" do
    before { @microtabla.content = " " }
    it { should_not be_valid }
  end

  describe "with content that is too long" do
    before { @microtabla.content = "a" * 20 }
    it { should_not be_valid }
  end
  
end


