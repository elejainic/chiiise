require 'rails_helper'
include Warden::Test::Helpers

describe "User page" do

	subject { page }

  describe "profile page" do
  
  
    let!(:user) { FactoryGirl.create(:user) }
    let!(:m1) { FactoryGirl.create(:microtabla, user: user, content: "Foo") }
    let!(:m2) { FactoryGirl.create(:microtabla, user: user, content: "Bar") }
    

    before { visit user_path(user) }

    it { should have_content(user.name) }
   
  

    describe "microtablas" do
      it { should have_content(m1.content) }
      it { should have_content(m2.content) }
      it { should have_content(user.microtablas.count) }
    end
   end
end