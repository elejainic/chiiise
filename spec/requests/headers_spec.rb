require 'spec_helper'
require 'rails_helper'

describe "Header" do

  describe "Front page" do
  	
  	it "should have the content 'Exodo'" do
      visit '/'
      expect(page).to have_content('Exodo')
    end

    it "should have the content 'Categorias'" do
      visit '/'
      expect(page).to have_content('Categorias')
    end

    it "should have the content 'Usuario'" do
      visit '/'
      expect(page).to have_content('Usuario')
    end
  end
end
