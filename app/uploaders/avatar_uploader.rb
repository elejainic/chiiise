# encoding: utf-8

class AvatarUploader < CarrierWave::Uploader::Base

  # Include RMagick or MiniMagick support:
  # include CarrierWave::RMagick
  # include CarrierWave::MiniMagick
  include Cloudinary::CarrierWave

  # Choose what kind of storage to use for this uploader:
  #storage :file
  #storage :fog
  
  #START FROM BASE64 POST LINKED ABOVE
   # class FilelessIO < StringIO
   #  attr_accessor :original_filename
   #  attr_accessor :tempfile
   #  attr_accessor :filename
   # end

  # before :cache, :convert_base64

  # def convert_base64(file)
  #   if file.respond_to?(:original_filename) &&
  #       file.original_filename.match(/^base64:/)
  #     fname = file.original_filename.gsub(/^base64:/, '')
  #     ctype = file.content_type
  #     decoded = Base64.decode64(file.read)
  #     file.file.tempfile.close!
  #     decoded = FilelessIO.new(decoded)
  #     decoded.original_filename = fname
  #     decoded.content_type = ctype
  #     file.__send__ :file=, decoded
  #   end
  #   file
  # end
#END FROM POST LINKED ABOVE


  # Override the directory where uploaded files will be stored.
  # This is a sensible default for uploaders that are meant to be mounted:
  # def store_dir
  #   "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  # end

  # Provide a default URL as a default if there hasn't been a file uploaded:
  # def default_url
  #   # For Rails 3.1+ asset pipeline compatibility:
  #   # ActionController::Base.helpers.asset_path("fallback/" + [version_name, "default.png"].compact.join('_'))
  #
  #   "/images/fallback/" + [version_name, "default.png"].compact.join('_')
  # end
   version :display do
     process :ager => true
    process :resize_to_fill => [100, 100, :north]
   end

  version :thumbnail do
    process :eager => true
    process :resize_to_fit => [330, 440]
  end

   version :thumb do
    process :eager => true
    process :resize_to_fit => [730, 840]
  end

  version :small do
    process :eager => true
    process :resize_to_fill => [250, 200]
  end

  version :tag do
    process :eager => true
    process :resize_to_fill => [30, 30, :north]
  end

    version :cartel do
    process :eager => true
    process :resize_to_fill => [420, 220]
  end
  # Process files as they are uploaded:
  # process :scale => [200, 300]
  #
  # def scale(width, height)
  #   # do something
  # end

  # Create different versions of your uploaded files:
  # version :thumb do
  #   process :resize_to_fit => [50, 50]
  # end

  # Add a white list of extensions which are allowed to be uploaded.
  # For images you might use something like this:
   # def extension_white_list
   #  %w(jpg jpeg gif png)
   # end

  # Override the filename of the uploaded files:
  # Avoid using model.id or version_name here, see uploader/store.rb for details.
  # def filename
  #   "something.jpg" if original_filename
  # end
  # def filename
  #    if original_filename
  #    Time.new.to_i.to_s+"_"+original_filename
  #    end
  #  end

end
