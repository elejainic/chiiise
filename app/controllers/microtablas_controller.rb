class MicrotablasController < ApplicationController
  respond_to :html, :js
  
  before_action :authenticate_user!, only: [:create, :destroy]
  before_action :correct_user,   only: :destroy

	def new
    @microtabla = current_user.microtablas.build
	end

  def show
    @microtabla = Microtabla.find(params[:id])
    @membership = @microtabla.memberships.all
    @post =  @microtabla.posts.all
    @member = Membership.find_by(:microtabla_id => @microtabla )
    @part = Invitacion.find_by(:microtabla_id => @microtabla )

    
  end

	  def create
    @microtabla = current_user.microtablas.build(microtabla_params)
    if @microtabla.save
      redirect_to user_path(current_user) 
    else
     redirect_to user_path(current_user)
    end
  end
  def edit
    @microtabla = current_user.microtablas.find(params[:id])
  end

  def update
    @microtablas = Microtabla.all
    @microtabla = Microtabla.find(params[:id])
    
    @microtabla.update_attributes(microtabla_params)
    redirect_to @microtabla
  end

  def destroy
    @microtabla.destroy
    redirect_to user_path(current_user)
  end

  private

  def microtabla_params
    params.require(:microtabla).permit(:content, :descripcion)
  end

  def membership_params
    params.require(:membership).permit(:microtabla_id, :user_id)
  end

  def correct_user
      @microtabla = current_user.microtablas.find_by(id: params[:id])
      redirect_to redirect_to user_path(current_user) if @microtabla.nil?
  end

   def invitacion_params
    params.require(:invitacion).permit(:contacto, :user_id, :microtabla_id)
  end
end
