class PostsController < ApplicationController
	before_action :authenticate_user!
  respond_to :html, :js

	def new
    @microtabla = Microtabla.find(params[:microtabla_id])
    @post = @microtabla.posts.build
	end

  def edit
    @microtabla = Microtabla.find(params[:microtabla_id])
    @post = current_user.posts.find(params[:id])
  end

  def update
    @post = Post.find(params[:id])
    @post.update_attributes(post_params)
    redirect_to microtabla_path(@post.microtabla_id)
  end

	 def create

    @post = current_user.posts.build(post_params)
    if @post.save
      redirect_to microtabla_path(@post.microtabla_id) 
    else
     redirect_to user_path(current_user)
    end
  end

   def destroy
      
    @post = current_user.posts.find(params[:id])
    @post.destroy
          redirect_to microtabla_path(@post.microtabla_id) 
  end

    private

    def post_params
    params.require(:post).permit(:member_id, :description, :image, :user_id, :microtabla_id)
  end
end
