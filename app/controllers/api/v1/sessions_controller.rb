class API::V1::SessionsController < ApplicationController

   after_action :set_access_control_headers
   respond_to :json

   
	def create
    @user_password = params[:sessions][:password]
    @user_email = params[:sessions][:email]
    
   if (User.find_by(email: @user_email)!= nil)
   	@user = @user_email.present? && User.find_by(email: @user_email)
    if @user.valid_password?(@user_password)
      sign_in @user, store: false 
      @user.generate_authentication_token!
      @user.save
      respond_to do |format|
  format.json { render json: @user.to_json}
  format.xml
  end 
    else
      render json: { errors: "Invalid email or password" }, status: 422
    end
else
	render json: { errors: "Invalid email or password" }, status: 422
end
  end

    def destroy
    user = User.find_by(auth_token: params[:id])
    user.generate_authentication_token!
    user.save
    head 204
  end

end

private
  def set_access_control_headers
   headers['Access-Control-Allow-Origin'] = "*"
   headers['Access-Control-Request-Method'] = %w{GET POST OPTIONS}.join(",")
 end