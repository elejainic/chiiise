class API::V1::MicrotablasController < ApplicationController
after_action :set_access_control_headers
   respond_to :json 

     def index
     @microtablas = Microtabla.first
    #@microtablas = Microtabla.all
    respond_to do |format|
  format.json { render json: @microtablas.to_json}
  format.xml
end
end

   def show
    parametros = params[:microtabla][:user_id]
    if params[:microtabla][:tipo] == "primero"
      @micro = Microtabla.where(user_id: parametros)
      @microtablas = @micro.first
      respond_to do |format|
        format.json { render json: @microtablas.to_json}
        format.xml
      end
    else
      @microtablas = Microtabla.where(:user_id => parametros)
      @user = User.find_by(:id => parametros)
      invitacions = Invitacion.where(:contacto => @user.email)
      #@microtablas = Microtabla.all
      @album = Microtabla.where(:user_id => parametros)
      part =[]
      @microtablas.each do |macro|
        macro.avatar = @user.avatar 
        #@avatar = User.find_by(:id => cara.user_id)
        part << macro  
      end
      invitacions.each do |macro|
        cara = Microtabla.find_by(:id => macro.microtabla_id)
        @avatar = User.find_by(:id => cara.user_id)
        cara.avatar = @avatar.avatar
        part << cara 
      end
      respond_to do |format|
        format.json { render json: part.to_json}
        format.xml
      end
    end 
  end
  

    


    def create
    	@microtabla = Microtabla.new(microtabla_params)
    	if @microtabla.save
    		respond_to do |format|
    			format.json { render json: @microtabla.to_json}
    			format.xml 
    		end
    	else
    		render json: { errors: @microtabla.errors }, status: 422
    	end
    end

  def destroy
    
    @microtabla = Microtabla.find(params[:id])
    @microtabla.destroy
    respond_to do |format|
          format.json { render json: @microtabla.to_json}
          format.xml 
        end
  end

end

 private
 def set_access_control_headers
   headers['Access-Control-Allow-Origin'] = "*"
   headers['Access-Control-Request-Method'] = %w{GET POST OPTIONS}.join(",")
 end

 def microtabla_params
    params.require(:microtabla).permit(:content, :user_id, :tipo, :avatar, :nombreuser, :link)
  end

