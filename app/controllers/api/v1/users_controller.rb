
class API::V1::UsersController < ApplicationController
  after_action :set_access_control_headers
   respond_to :json  

  def show
    parametros = params[:user][:link]
    @user = User.find_by(:id => parametros)
    #@users = User.second
    respond_to do |format|
  format.json { render json: @user.to_json}
  format.xml
end
  end

    def create
    @user = User.new(user_params)
    @user.generate_authentication_token!
    if @user.save
      respond_to do |format|
  format.json { render json: @user.to_json}
  format.xml 
end
    else
      render json: { errors: @user.errors }, status: 422
    end
  end
end

 private
 def set_access_control_headers
   headers['Access-Control-Allow-Origin'] = "*"
   headers['Access-Control-Request-Method'] = %w{GET POST OPTIONS}.join(",")
 end

  def user_params
      params.require(:user).permit(:name, :email, :password, :password_confirmation, :link)
    end
