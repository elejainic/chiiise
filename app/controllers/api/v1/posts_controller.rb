class API::V1::PostsController < ApplicationController

 after_action :set_access_control_headers
   respond_to :json 

   
   
	def show
		@posts = Post.all
    # parametros = params[:post][:microtabla_id]
    
		 #@posts = Post.where(microtabla_id: parametros)
    #@microtablas = Microtabla.all
    respond_to do |format|
  format.json { render json: @posts.to_json}
  format.xml
end
end

    def create
    @post = Post.new(post_params)
      if @post.save
        respond_to do |format|
          format.json { render json: @post.to_json}
          format.xml 
        end
      else
        render json: { errors: @post.errors }, status: 422
      end
    end
end

private
def set_access_control_headers
   headers['Access-Control-Allow-Origin'] = "*"
   headers['Access-Control-Request-Method'] = %w{GET POST OPTIONS}.join(",")
 end
    def post_params
    the_params = params.require(:post).permit(:image , :user_id, :microtabla_id)
    the_params[:image] = parse_image_data(the_params[:image]) if the_params[:image]
    the_params
  end




     def parse_image_data(base64_image)
filename = "upload-image"
in_content_type, encoding, string = base64_image.split(/[:;,]/)[1..3]
 
@tempfile = Tempfile.new(filename)
@tempfile.binmode
@tempfile.write Base64.decode64(string)
@tempfile.rewind
 
# for security we want the actual content type, not just what was passed in
content_type = `file --mime -b #{@tempfile.path}`.split(";")[0]
 
# we will also add the extension ourselves based on the above
# if it's not gif/jpeg/png, it will fail the validation in the upload model
extension = content_type.match(/gif|jpeg|png/).to_s
filename += ".#{extension}" if extension
 
ActionDispatch::Http::UploadedFile.new({
tempfile: @tempfile,
content_type: content_type,
filename: filename
}) 
  end

   def clean_tempfile
if @tempfile
@tempfile.close
@tempfile.unlink
end
end


