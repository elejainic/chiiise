class API::V1::InvitacionsController < ApplicationController

 after_action :set_access_control_headers
   respond_to :json

    

    def create
      @contact = params[:invitacion][:contacto]
      @convite = Invitacion.new(invitacion_params)
      
      if @convite.save
       respond_to do |format|
          format.json { render json: @convite.to_json}
          format.xml 
        end
      else
        render json: { errors: @convite.errors }, status: 422
      end
     	

     	WelcomeMailer.registration_confirmation(@contact, @convite).deliver 
    end
      
      
    # else
    #  redirect_to user_path(current_user)
  #end
    #end
  
end
 private
 def set_access_control_headers
   headers['Access-Control-Allow-Origin'] = "*"
   headers['Access-Control-Request-Method'] = %w{GET POST OPTIONS}.join(",")
 end

def invitacion_params
    params.require(:invitacion).permit(:contacto, :user_id, :microtabla_id)
  end