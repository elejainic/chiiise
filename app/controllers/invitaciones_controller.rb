class InvitacionesController < ApplicationController

	def new
		@microtabla = Microtabla.find(params[:microtabla_id])
        @presentacion = Invitacion.new

	end

def invitacion_params
    params.require(:invitacion).permit(:contacto, :user_id, :microtabla_id)
  end
end
