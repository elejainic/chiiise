class MembershipsController < ApplicationController
	before_action :authenticate_user!
  rescue_from PG::Error, with: :pg_error

    def create
    @member = current_user.memberships.build(membership_params)
   
    if @member.save
      redirect_to microtabla_path(@member.microtabla_id)
# WelcomeMailer.registration_confirmation(current_user).deliver 
    else
     redirect_to user_path(current_user)
    end
  end

    def destroy
    	
    @membership = current_user.memberships.find(params[:id])
    @membership.destroy
          redirect_to microtabla_path(@membership.microtabla_id) 

    end	

    def pg_error
      redirect_to microtabla_path(@member.microtabla_id)
    end

  def membership_params
    params.require(:membership).permit(:microtabla_id, :user_id, :name, :email, :avatar)
  end
end
