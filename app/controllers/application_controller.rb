class ApplicationController < ActionController::Base
  before_action :configure_devise_params, if: :devise_controller?
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with:  :null_session, if: Proc.new { |c| c.request.format == 'application/json' }
  skip_before_filter :verify_authenticity_token, :if => Proc.new { |c| c.request.format == 'application/json' }
  after_filter :store_location
  layout 'application'
  
def store_location
  # store last url - this is needed for post-login redirect to whatever the user last visited.
  return unless request.get? 
  if (request.path != "/users/sign_in" &&
      request.path != "/users/sign_up" &&
      request.path != "/users/password/new" &&
      request.path != "/users/password/edit" &&
      request.path != "/users/confirmation" &&
      request.path != "/users/sign_out" &&

      !request.xhr?) # don't store ajax calls
    session[:previous_url] = request.fullpath 
  end
end

def after_sign_in_path_for(resource)
  if (session[:previous_url] == root_path)
    user_path(current_user)
  else
  session[:previous_url]
end
end

    def disable_nav
    	@disable_nav = true
    end

    

    def after_sign_up_path_for(resource)
    	user_path(current_user)
    end

      def configure_devise_params
      devise_parameter_sanitizer.for(:sign_up) { |u| u.permit(:name, :email, :password, :password_confirmation, :remember_me, :avatar) }
      devise_parameter_sanitizer.for(:account_update) << :name 
      devise_parameter_sanitizer.for(:account_update) << :avatar
  end
end
