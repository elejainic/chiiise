 class InvitacionsController < ApplicationController
  respond_to :html, :js
		
    def new
		@microtabla = Microtabla.find(params[:microtabla_id])
    @presentacion = @microtabla.invitacions.build
    end

  def show
    @microtabla = Microtabla.find(params[:id])
    @user = User.find_by(:id => @microtabla.user_id )
    @people = @microtabla.invitacions.all
    @gente = @people.pluck(:contacto).uniq

   end

	 def create

    @invitacion = current_user.invitacions.build(invitacion_params)
     # if @invitacion.save
     @enviar = @invitacion.contacto.split
     @enviar.each do |p|
      # @invitacion.contacto = p
      # @invitacion.save
      @convite = Invitacion.new(invitacion_params)
      @convite.contacto = p
      @convite.save
     	q = @invitacion

     	WelcomeMailer.registration_confirmation(p,q).deliver 
    end
      
      redirect_to microtabla_path(@invitacion.microtabla_id) 
    # else
    #  redirect_to user_path(current_user)
  #end
    #end
  
end
def invitacion_params
    params.require(:invitacion).permit(:contacto, :user_id, :microtabla_id)
  end
end

