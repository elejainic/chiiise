class UsersController < ApplicationController
  respond_to :html, :js

  def show
  	@user = User.find(params[:id])
  	@microtablas = current_user.microtablas.all
  	@invitacion = Invitacion.all
  	@marco = Microtabla.all

  end

end
