class Invitacion < ActiveRecord::Base
	belongs_to :user
	belongs_to :microtabla

	accepts_nested_attributes_for :microtabla
end
