class Microtabla < ActiveRecord::Base
	belongs_to :user
	has_many :memberships, dependent: :destroy
	has_many :posts, dependent: :destroy
	has_many :invitacions
	accepts_nested_attributes_for :posts

	default_scope -> { order('created_at DESC') }
	validates :content, presence: true, length: { maximum: 30 }

	validates :user_id, presence: true
end
