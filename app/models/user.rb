class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  mount_uploader :avatar, AvatarUploader

  # validates :auth_token, uniqueness: true

  has_many :microtablas, dependent: :destroy 
  has_many :memberships, dependent: :destroy
  has_many :posts, dependent: :destroy
  has_many :invitacions

  def generate_authentication_token!
    begin
      self.auth_token = Devise.friendly_token
    end while self.class.exists?(auth_token: auth_token)
  end

  before_create :generate_authentication_token!

  def skip_confirmation!
  self.confirmed_at = Time.now
end

end
