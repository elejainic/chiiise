class Post < ActiveRecord::Base
	belongs_to :user, class_name: 'User', foreign_key: 'user_id'
	belongs_to :membership
	belongs_to :microtabla
	mount_uploader :image, AvatarUploader

	accepts_nested_attributes_for :membership
end
