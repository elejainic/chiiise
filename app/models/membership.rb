class Membership < ActiveRecord::Base
	belongs_to :microtabla
    belongs_to :user
    has_many :posts

    accepts_nested_attributes_for :user
    
    # validates :user_id, uniqueness: true
    
end
