class CustomRedirection < Devise::FailureApp
   def redirect_url
     #Your custom redirect path goes here, do whatever you need here
   end
   def respond
     if http_auth?
       http_auth
     else
       redirect
     end
   end
 end