class AddImageToMicrotablas < ActiveRecord::Migration
  def change
    add_column :microtablas, :image, :string
  end
end
