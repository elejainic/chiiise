class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.integer :member_id
      t.string :description
      t.string :image

      t.timestamps
    end
  end
end
