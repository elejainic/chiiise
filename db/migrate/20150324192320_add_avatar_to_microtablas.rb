class AddAvatarToMicrotablas < ActiveRecord::Migration
  def change
    add_column :microtablas, :avatar, :string
  end
end
