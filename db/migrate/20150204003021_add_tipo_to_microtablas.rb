class AddTipoToMicrotablas < ActiveRecord::Migration
  def change
    add_column :microtablas, :tipo, :string
  end
end
