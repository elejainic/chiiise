class AddFilenameToPosts < ActiveRecord::Migration
  def change
    add_column :posts, :filename, :string
  end
end
