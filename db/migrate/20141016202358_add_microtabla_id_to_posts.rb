class AddMicrotablaIdToPosts < ActiveRecord::Migration
  def change
    add_column :posts, :microtabla_id, :integer
  end
end
