class CreateMicrotablas < ActiveRecord::Migration
  def change
    create_table :microtablas do |t|
      t.string :content
      t.integer :user_id

      t.timestamps
    end
    add_index :microtablas, [:user_id, :created_at]
  end
end
