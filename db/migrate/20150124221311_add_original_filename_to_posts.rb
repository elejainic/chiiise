class AddOriginalFilenameToPosts < ActiveRecord::Migration
  def change
    add_column :posts, :original_filename, :string
  end
end
