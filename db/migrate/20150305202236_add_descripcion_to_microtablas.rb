class AddDescripcionToMicrotablas < ActiveRecord::Migration
  def change
    add_column :microtablas, :descripcion, :string
  end
end
