class AddLinkToMicrotablas < ActiveRecord::Migration
  def change
    add_column :microtablas, :link, :integer
  end
end
