class CreateInvitacions < ActiveRecord::Migration
  def change
    create_table :invitacions do |t|
      t.string :contacto

      t.timestamps
    end
  end
end
