class AddIndexMembershipsMicrotablaUserId < ActiveRecord::Migration
  def change
   add_index :memberships, :microtabla_id
   add_index :memberships, :user_id
   add_index :memberships, [:microtabla_id, :user_id], unique: true
  end
end
